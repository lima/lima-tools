#!/bin/bash

beginw=$1
endw=$2
beginh=$3
endh=$4
step=$5

for i in $(seq $beginw $step $endw); do
    for j in $(seq $beginh $step $endh); do
	./test.out $i $j
	if [ $? != 0 ]; then
	   echo "fail $i x $j"
	fi
    done
    echo "done $i"
done
